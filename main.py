"""
Convert XLS file to range of CSV files, one for every sheet book.
main.py -i "" -o "" -e "" -d ""
    i: input xls path;
    o: output xls path;
    e: error log path;
    d: delimiter, default is ',';
"""

import csv
import logging
import os
import xlrd
from argparse import ArgumentParser
from sys import maxsize

ARGS = {}


def assert_files_are_valid():
    """ Check arg files are valid """
    input_file = ARGS.in_file
    output_file = ARGS.out_file

    if not os.path.exists(input_file):
        raise Exception("Input file: {} doesnt exist".format(input_file))
    elif os.path.isdir(input_file):
        raise Exception("Input file: {} is directory".format(input_file))
    elif os.path.isdir(output_file):
        raise Exception("Output file: {} is directory".format(output_file))


def assert_row_limits_is_valid(value: list, sheets_count: int):
    """ Check row_limits argument is valid for input xls. """
    if len(value) != sheets_count:
        raise Exception(
            "row-limits have {rl_count} elements: [{rl_values}], but xls have {sheets_count}".format(
                rl_count=len(value),
                rl_values=ARGS.row_limits,
                sheets_count=sheets_count,
            )
        )


def process_row_value(row_value: object) -> object:
    """ Process each cell in sheets. """
    if isinstance(row_value, str):
        # For strings remove internal newline chars.
        return row_value \
            .replace('\r', '') \
            .replace('\n', '') \
            .replace('"', '')
    else:
        return row_value


def convert():
    """ Main function. """
    assert_files_are_valid()

    input_file = ARGS.in_file
    output_file = ARGS.out_file

    with xlrd.open_workbook(input_file) as workbook:
        if ARGS.row_limits:
            row_limits = []
            try:
                for r in ARGS.row_limits.split(','):
                    v = r.split(':')
                    row_limits.append(
                        (int(v[0]), maxsize if v[1] == '0' else int(v[1]) + 1),
                    )
            except Exception as exc:
                raise Exception("Bad row-limits argument: {}".format(str(exc)))
            assert_row_limits_is_valid(row_limits, workbook.nsheets)
        else:
            row_limits = [(0, maxsize) for _ in range(workbook.nsheets)]

        file_path, extension = os.path.splitext(output_file)

        for sheet_idx in range(workbook.nsheets):
            sheet = workbook.sheet_by_index(sheet_idx)
            file_name = '{path}{index}{ext}'.format(
                path=file_path,
                index=sheet_idx,
                ext=extension,
            )
            # Ignore first |row_idx_to_start| rows.
            from_row, to_row = row_limits[sheet_idx]

            with open(file_name, 'w', encoding='utf-8', newline='') as f:
                writer = csv.writer(f, delimiter=ARGS.delim)
                for j in range(from_row, min(sheet.nrows, to_row)):
                    rows = [process_row_value(row) for row in sheet.row_values(j)]
                    writer.writerow(rows)


if __name__ == '__main__':
    parser = ArgumentParser()
    parser.add_argument(
        '-i',
        action='store',
        dest='in_file',
        help='Input file path',
        required=True
    )
    parser.add_argument(
        '-o',
        action='store',
        dest='out_file',
        help='Output file path',
        required=True
    )
    parser.add_argument(
        '-e',
        action='store',
        dest='err_file',
        help='Error logs file path',
        required=True
    )
    parser.add_argument(
        '-d',
        action='store',
        dest='delim',
        default=',',
        help='CSV delimiter. "," is default',
        required=False,
    )
    parser.add_argument(
        '-rl',
        action='store',
        dest='row_limits',
        default=None,
        help='Format: "start:end,start:end". Array for each sheet shows amount rows from beginning to skip',
        required=False,
    )
    ARGS = parser.parse_args()

    logging.basicConfig(
        format='%(filename)s[LINE:%(lineno)d]# %(levelname)-8s [%(asctime)s]  %(message)s',
        filename=ARGS.err_file,
        filemode='a',
        level=logging.INFO,
    )

    try:
        convert()
    except Exception as exc:
        print("Script failed: " + str(exc))
        logging.error(str(exc))
        exit(0)
    exit(1)
